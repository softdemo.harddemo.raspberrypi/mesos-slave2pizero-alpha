#!/bin/sh

if [ $# -ne 1 ]; then
  echo "*** ERROR ***"
  echo "Usage : install-mesos2pizero.sh MESOS-MASTER-IP-ADDRESS"
  echo "  ex) install-mesos2pizero.sh 192.168.10.11"
  echo "Exit by code 1 (Terminated)."
  exit 1
fi
MASTER_PORT=5050
MASTER_IP_ADDRESS=$1
echo "Setting up mesos-slave for master ($MASTER_IP_ADDRESS:$MASTER_PORT)..."

SCRIPTDIR=./sbin

# install external libs
if [ "x$ONLY_UPDATE_FILES" = "x" ]; then
  echo 
  echo "Installing external libs..."
  sudo $SCRIPTDIR/install-ext-libs.sh
else
  echo "Skipping to install external libs."
fi 

# unpack tgz file to /usr/local
echo "Unpacking files"
sudo $SCRIPTDIR/unpack.sh

# create mesos user, group
if [ "x$ONLY_UPDATE_FILES" = "x" ]; then
  echo
  echo "Creating mesos user and group..."
  sudo $SCRIPTDIR/group-user-add.sh
else
  echo "Skipping to create mesos user, group."
fi

# setup env file
if [ "x$ONLY_UPDATE_FILES" = "x" ]; then
  echo 
  echo "Setting up environment file.."
  sudo $SCRIPTDIR/setup-env.sh $MASTER_IP_ADDRESS
else
  echo "Skipping to setup env file."
fi

# setup service
if [ "x$ONLY_UPDATE_FILES" = "x" ]; then
  echo 
  sudo $SCRIPTDIR/setup-service.sh
else
  echo "Skipping to setup service."
fi


# finish
echo 
echo "Set up finished."


exit 0
