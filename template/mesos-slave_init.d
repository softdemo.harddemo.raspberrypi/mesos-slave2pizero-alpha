#!/bin/sh
### BEGIN INIT INFO
# Provides:          mesos-slave
# Required-Start:    $network $local_fs $named $remote_fs $syslog 
# Required-Stop:     $network $local_fs $named $remote_fs $syslog
# Should-Start:      $time
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description:
# Description:
### END INIT INFO

case "$1" in
  start)
    sudo -u mesos mesos-daemon.sh mesos-slave --no-systemd_enable_support
    ;;
  stop)
    killall mesos-slave
    ;;
  *)
    echo "Usage: service mesos-slave {start|stop}" >&2
    exit 1
    ;;
esac

exit 0
