Apache Mesos slave to Raspberry Pi Zero
====

※Raspbian Jessie のバージョンは8.0 (ClusterHAT導入時のバージョン）

Raspberry Pi ZeroにApache Mesosのslave(Agent)を導入するためのインストーラーです。
Mesosのバージョンは1.0.0で、ビルドはRaspberry Pi 3 Jsseie (8.0) で行っています。
Raspberry Pi 3 でビルド・インストールした後に必要そうなファイルをとりあえず見繕って収録したものなのであくまでもアルファ版とします。
ご利用される方はあくまでも自己責任でお願いします。
なお、ClusterHATで使用するRaspberry Pi Zero用を想定しており、それ以外の環境での動作確認は一切行っておりませんのであらかじめご了承ください。

## 使い方

	./install-mesos2pizero.sh MESOS-MASTER-IP-ADDRESS
	
	(2017.3.5)モジュールファイルの修正を行いました。
	既にセットアップ済で修正ファイルのみを反映したい場合は上記コマンド実行前に
	export ONLY_UPDATE_FILES=1
	を実行してから上記コマンドを実行してください。モジュールのコピー以外の処理をスキップします。
	
MESOS-MASTER-IP-ADDRESSにはClusterHATのマスター機であるRaspbery Pi 2B/3の外部からアクセス可能なインターフェースのIPアドレスを指定してください。

## 改定履歴
2017.3.5 /usr/local/libexec以下のファイルをmesos-1.0.0-pizero-alpha.tgzに追加した（Marathon-Docker連携で問題発生）。またそれに伴いスクリプトも修正しました。	

