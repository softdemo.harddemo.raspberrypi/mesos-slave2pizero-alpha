#!/bin/sh

TEMPLATEFILE=./template/mesos-slave-env.sh
ENVFILE=/usr/local/etc/mesos/mesos-slave-env.sh
MASTER_IP_ADDRESS=$1
MY_IP_ADDRESS=`hostname -I`

sed "s/<<MASTER IP ADDRESS>>/$MASTER_IP_ADDRESS/g" $TEMPLATEFILE | \
sed "s/<<MY IP ADDRESS>>/$MY_IP_ADDRESS/g" > $ENVFILE
chown mesos.mesos $ENVFILE

exit 0
