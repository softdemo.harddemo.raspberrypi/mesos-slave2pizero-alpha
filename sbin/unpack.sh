#!/bin/sh

CWD=`pwd`
TGZFILE=$CWD/mesos-1.0.0-pizero-alpha.tgz
TARGETDIR=/usr/local

echo "Unpacking $TGZFILE to $TARGETDIR ..."
cd $TARGETDIR; tar zxvf $TGZFILE

ldconfig

exit 0
