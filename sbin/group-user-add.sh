#!/bin/bash

# create mesos user and group
# change owner/group of installed mesos files to mesos
# make some symbolic links
echo "Adding group mesos..."
groupadd mesos
echo "Adding user mesos..."
useradd -g mesos -d /home/mesos -s /bin/bash -m mesos
if [ ! -e /var/lib/mesos ]; then
  echo "Making directory /var/lib/mesos..."
  mkdir /var/lib/mesos
fi
echo "Changing owner for /var/lib/mesos to mesos..."
chown -R mesos.mesos /var/lib/mesos
if [ -e /home/mesos/work ]; then
  /bin/rm -f /home/mesos/work
fi
echo "Making symbolic link /home/mesos/work for /var/lib/mesos.."
sudo -u mesos ln -s /var/lib/mesos /home/mesos/work
if [ ! -e /var/log/mesos ]; then
  echo "Making directory /var/log/mesos..."
  mkdir /var/log/mesos
fi
echo "Changing owner for /var/log/mesos to mesos..."
sudo chown -R mesos.mesos /var/log/mesos
if [ -e /home/mesos/log ]; then
  /bin/rm -f /home/mesos/log
fi
echo "Making symbolic link /home/mesos/log for /var/log/mesos.."
sudo -u mesos ln -s /var/log/mesos /home/mesos/log
if [ -e /home/mesos/sbin ]; then
  /bin/rm -f /home/mesos/sbin
fi
echo "Making symbolic link /home/mesos/sbin for /usr/local/sbin.." 
sudo -u mesos ln -s /usr/local/sbin /home/mesos/sbin
if [ -e /home/mesos/etc ]; then
  /bin/rm -f /home/mesos/etc
fi
echo "Making symbolic link /home/mesos/etc for /usr/local/etc/mesos.."
sudo -u mesos ln -s /usr/local/etc/mesos /home/mesos/etc
chown -R mesos.mesos /usr/local/etc/mesos 

exit 0
