#!/bin/sh

TEMPLATEDIR=./template

echo "Setting up 'mesos-slave' service..."
cp $TEMPLATEDIR/mesos-slave_init.d /etc/init.d/mesos-slave
chmod a+x /etc/init.d/mesos-slave

echo "Registering service mesos-slave..."
insserv -d mesos-slave

echo "Starting mesos-slave service..."
/etc/init.d/mesos-slave start

